package com.titanium.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.titanium.controller.TaskController;
import com.titanium.model.TaskModel;
import com.titanium.model.UserModel;

public class TaskService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = null;
		
		if(!request.getParameter("action").trim().equals(null)){
			action = request.getParameter("action").trim();
		}
		
		if(action.equals("Update")){
			
			int id = Integer.parseInt(request.getParameter("id").trim());
			int status = Integer.parseInt(request.getParameter("status").trim());
			
			TaskModel model = new TaskModel();
			
			model.setId(id);
			model.setStatus(status);
			
			TaskController control = new TaskController();
			
			control.updateTask(model);
			
		}else if(action.equals("Delete")){
			
			int id = Integer.parseInt(request.getParameter("id").trim());
			
			TaskModel model = new TaskModel();
			
			model.setId(id);
			
			TaskController control = new TaskController();
			
			control.deleteTask(model);
			
		}
		
		if(!request.getParameter("task").trim().equals(null) && action.equals("Register")){
			
			response.setContentType("application/json");
			
			String task = request.getParameter("task").trim();
			UserModel user = (UserModel) request.getSession().getAttribute("employeeSession");
			
			TaskModel model = new TaskModel(task);
			
			model.setDescription(task);
			model.setUserId(user.getId());
			
			TaskController control = new TaskController();
	
			int currentHighestId = control.newTask(model);
			
			String searchList = new Gson().toJson(currentHighestId);
			response.getWriter().write(searchList);
		}

	}
}