package com.titanium.service;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.titanium.controller.UserController;
import com.titanium.model.UserModel;

public class LoginService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher = null;
		
		if(!request.getParameter("email").trim().equals(null) && !request.getParameter("password").trim().equals(null) ){
			String email = request.getParameter("email").trim();
			String password = request.getParameter("password").trim();
			
			UserModel user = new UserModel(email, password);
			UserController control = new UserController();
			if(control.verifyUserAndPassword(user)){
				HttpSession session = request.getSession();
				requestDispatcher = request.getRequestDispatcher("/home.jsp");
				
				session.setAttribute("employeeSession", user);
			}else{
				requestDispatcher = request.getRequestDispatcher("/login.jsp");
				request.setAttribute("error", "The email or password is incorrect.");
				request.setAttribute("email", email);
			}
		}else{
			requestDispatcher = request.getRequestDispatcher("/login.jsp");
			request.setAttribute("error", "Please fill all the fields above.");
		}
		
		requestDispatcher.include(request, response);

	}
}