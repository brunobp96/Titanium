package com.titanium.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.titanium.controller.UserController;
import com.titanium.model.UserModel;

public class UserService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/json");
		
//		String action = request.getParameter("action").trim();
		String email = request.getParameter("email").trim();
		String password = request.getParameter("password").trim();
		
		UserModel user = new UserModel(email, password);
		UserController control = new UserController();

		String searchList = new Gson().toJson(control.newUser(user));
		response.getWriter().write(searchList);

	}
}