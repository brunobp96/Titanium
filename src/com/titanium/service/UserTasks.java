package com.titanium.service;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.titanium.controller.TaskController;
import com.titanium.model.TaskModel;
import com.titanium.model.UserModel;

public class UserTasks extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		UserModel user = (UserModel) request.getSession().getAttribute("employeeSession");
		
		TaskController control = new TaskController();
		
		ArrayList<TaskModel> taskList = new ArrayList<TaskModel>();
		
		control.getUserTasks(user,taskList);
		
		request.setAttribute("taskList", taskList);
		
	}
}