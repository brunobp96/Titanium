package com.titanium.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.titanium.model.TaskModel;
import com.titanium.model.UserModel;

public class TaskRepository extends DBFactory {
	
	public int newTask(TaskModel model) throws SQLException{

		ResultSet resultSet = null;
		connection = getConnection();
		java.sql.PreparedStatement statement;

		String sql = "insert into task(taskDescription,taskStatus,userId) values(?,?,?)";

		statement = connection.prepareStatement(sql);

		statement.setString(1, model.getDescription());
		statement.setInt(2, 1);
		statement.setInt(3, model.getUserId());
		
		statement.executeUpdate();
		
		
		sql = "select max(taskId) from task";

		statement = connection.prepareStatement(sql);
		resultSet = statement.executeQuery();

		int higestId = 0;
		while (resultSet.next()) {
			higestId = resultSet.getInt(1);
		}

		statement.close();
		connection.close();
		
		return higestId;

	}

	public void getUserTasks(UserModel user, ArrayList<TaskModel> taskList) throws SQLException {

		ResultSet resultSet;
		connection = getConnection();
		java.sql.PreparedStatement statement;

		String sql = "select taskId, taskDescription, taskStatus from task where userId = ?";

		statement = connection.prepareStatement(sql);

		statement.setInt(1, user.getId());
		
		resultSet = statement.executeQuery();
		
		while (resultSet.next()) {
			TaskModel model = new TaskModel();

			model.setId(resultSet.getInt(1));
			model.setDescription(resultSet.getString(2));
			model.setStatus(resultSet.getInt(3));

			taskList.add(model);
		}

		statement.close();
		connection.close();
		
	}

	public void updateTask(TaskModel model) throws SQLException {
		connection = getConnection();
		java.sql.PreparedStatement statement;

		String sql = "update task set taskStatus = ? where taskId = ?";

		statement = connection.prepareStatement(sql);

		statement.setInt(1, model.getStatus());
		statement.setInt(2, model.getId());
		
		statement.executeUpdate();
	}

	public void deleteTask(TaskModel model) throws SQLException {
		connection = getConnection();
		java.sql.PreparedStatement statement;

		String sql = "delete from task where taskId = ?";

		statement = connection.prepareStatement(sql);

		statement.setInt(1, model.getId());
		
		statement.executeUpdate();
	}
	
}
