package com.titanium.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.titanium.model.UserModel;


public class UserRepository  extends DBFactory  {

	public void newUser(UserModel model) throws SQLException{

		connection = getConnection();
		java.sql.PreparedStatement statement;

		String sql = "insert into user(userEmail,userPassword) values(?,?)";

		statement = connection.prepareStatement(sql);

		statement.setString(1, model.getEmail());
		statement.setString(2, model.getPassword());
	

		statement.executeUpdate();

		statement.close();
		connection.close();

	}

	public boolean verifyUserExistence(UserModel model) throws SQLException {
		connection = getConnection();
		PreparedStatement statement;
		ResultSet resultSet;
		boolean result = true;

		String sql = "select userId from user where userEmail = ?";

		statement = connection.prepareStatement(sql);

		statement.setString(1, model.getEmail());

		resultSet = statement.executeQuery();
		
		while (resultSet.next()) {
			if(resultSet.getInt(1) >= 1){
				result = false;
			}
		}
		
		statement.close();
		connection.close();
		
		return result;
	}

	public boolean verifyUserAndPassword(UserModel user) throws SQLException {
		connection = getConnection();
		PreparedStatement statement;
		ResultSet resultSet;
		boolean result = false;

		String sql = "select userId from user where userEmail = ? and userPassword = ?";

		statement = connection.prepareStatement(sql);

		statement.setString(1, user.getEmail());
		statement.setString(2, user.getPassword());

		resultSet = statement.executeQuery();
		
		while (resultSet.next()) {
			if(resultSet.getInt(1) >= 1){
				user.setId(resultSet.getInt(1));
				result = true;
			}
		}
		
		statement.close();
		connection.close();
		
		return result;
	}

}
