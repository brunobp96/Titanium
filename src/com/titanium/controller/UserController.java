package com.titanium.controller;

import java.sql.SQLException;

import com.titanium.model.UserModel;
import com.titanium.repository.UserRepository;

public class UserController {

	public boolean newUser(UserModel model) {
		try {

			UserRepository dao = new UserRepository();
			
			if(!dao.verifyUserExistence(model)){
				return false;
			}else{
				dao.newUser(model);
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean verifyUserAndPassword(UserModel user) {
		try {

			UserRepository dao = new UserRepository();
			
			if(dao.verifyUserAndPassword(user)){
				return true;
			}else{
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
