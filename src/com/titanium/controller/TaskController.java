package com.titanium.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import com.titanium.model.TaskModel;
import com.titanium.model.UserModel;
import com.titanium.repository.TaskRepository;

public class TaskController {

	public int newTask(TaskModel model) {
		try {
			TaskRepository dao = new TaskRepository();

			return(dao.newTask(model));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void getUserTasks(UserModel user, ArrayList<TaskModel> taskList) {
		try {
			TaskRepository dao = new TaskRepository();

			dao.getUserTasks(user,taskList);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateTask(TaskModel model) {
		try {
			TaskRepository dao = new TaskRepository();

			dao.updateTask(model);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteTask(TaskModel model) {
		try {
			TaskRepository dao = new TaskRepository();

			dao.deleteTask(model);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}



}
