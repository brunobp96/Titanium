<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="type" required="true"%>
<%@ attribute name="value" required="false"%>
<%@ attribute name="icon" required="true"%>
<%@ attribute name="fieldName" required="true"%>
<%@ attribute name="displayName" required="false"%>
<%@ attribute name="toolTip" required="false"%>
<%@ attribute name="width" required="false"%>
<%@ attribute name="htmlAttributes" required="false"%>
<%@ attribute name="id" required="false"%>
<%@ attribute name="autocomplete" required="false"%>

<c:choose>
	<c:when test="${type == 'text'}">
		<div class="form-group <c:out escapeXml="true" default="col-sm-12" value="${empty width ? 'col-sm-12' : width}"/>">
			<label>${displayName}</label>
			<div class="input-group <c:out value="${autocomplete == 'true' ? 'ui-front' : ''}" />">
				<span class="input-group-addon" data-toggle="tooltip" data-placement="right" title="${toolTip}">
					<i class="fa ${icon}"></i>
				</span>
				<input type="text" id="${id}" class="form-control" name="${fieldName}" placeholder="${displayName}" value="${value}" ${htmlAttributes}/>
			</div>
		</div>
	</c:when>
	<c:when test="${type == 'password'}">
		<div class="form-group <c:out escapeXml="true" default="col-sm-12" value="${empty width ? 'col-sm-12' : width}"/>">
			<label>${displayName}</label>
			<div class="input-group">
				<span class="input-group-addon" data-toggle="tooltip" data-placement="right" title="${toolTip}">
					<i class="fa ${icon}"></i>
				</span>
				<input type="password" id="${id}" class="form-control" name="${fieldName}" placeholder="${displayName}" value="${value}" ${htmlAttributes}/>
			</div>
		</div>
	</c:when>
</c:choose>