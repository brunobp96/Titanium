<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="displayName" required="false"%>
<%@ attribute name="htmlAttributes" required="false"%>
<%@ attribute name="width" required="false"%>
<%@ attribute name="body" fragment="true" required="false"%>

<div class="form-group ${width}">
	<label>Comentário:</label>
	<textarea rows="6" class="form-control" name="${name}" placeholder="${displayName}" ${htmlAttributes}><jsp:invoke fragment="body" /></textarea>
</div>
