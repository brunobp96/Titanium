<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="h"%>
<%@ attribute name="modalId" required="true"%>
<%@ attribute name="title" required="false"%>
<%@ attribute name="confirmStyle" required="false"%>
<%@ attribute name="cancelStyle" required="false"%>
<%@ attribute name="body" fragment="true" required="false"%>
<%@ attribute name="target" required="false"%>

<div class="modal fade" id="${modalId}" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">${title}</h4>
			</div>
			<div class="modal-body">
				<jsp:invoke fragment="body" />
			</div>
			<div class="modal-footer">
				<button type="button" class="btn <c:out value="${empty cancelStyle ? 'btn-default' : cancelStyle}"></c:out>" data-dismiss="modal">Cancelar</button>
				<button id="confirm-${modalId}" type="button" class="btn <c:out value="${empty confirmStyle ? 'btn-primary' : confirmStyle}"></c:out>">Confirmar</button>
			</div>
		</div>
	</div>
</div>


<script>
	$("#confirm-<c:out value="${modalId}"></c:out>").on("click", function() {
		$("#<c:out value="${target}"></c:out>").submit();
	});
</script>
