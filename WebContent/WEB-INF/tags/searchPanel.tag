<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="formAction" required="true"%>
<%@ attribute name="collapsed" required="false"%>
<%@ attribute name="body" fragment="true" required="false"%>
<%@ attribute name="id" required="false"%>

<div class="os-panel" id="${id}">
	<div class="os-title">
		<h2>
			<i class="fa fa-filter"></i> Filtro
		</h2>
		<ul class="nav navbar-right panel-toolbox">
			<li>
				<a data-toggle="collapse" href="#search-form" aria-expanded="true" class="collapse-link">
					<i class="fa fa-chevron-up os-toggle"></i>
				</a>
			</li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div id="search-form" class="os-content collapse in">
		<form action="${formAction}">
			<jsp:invoke fragment="body" />
			<div class="row">
				<div class="clearfix"></div>
				<div class="hr-solid"></div>
			</div>
			<div class="form-group col-sm-12">
				<button type="submit" class="btn btn-primary" value="Pesquisar">
					<i class="fa fa-search"></i> Pesquisar
				</button>
			</div>
		</form>
	</div>
</div>