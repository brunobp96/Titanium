<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="body" fragment="true" required="true"%>
<%@ attribute name="displayName" required="false"%>
<%@ attribute name="width" required="false"%>
<%@ attribute name="icon" required="false"%>
<%@ attribute name="tooltip" required="false"%>
<%@ attribute name="id" required="false"%>

<div class="form-group <c:out escapeXml="true" default="col-sm-12" value="${empty width ? 'col-sm-12' : width}"/> pull-left">
	<label>${displayName}</label>
	<div class="input-group">
		<span class="input-group-addon" data-toggle="tooltip" data-placement="right" title="${tooltip}">
			<i class="fa ${icon}"></i>
		</span>
		<select class="form-control" name="${name}" id="${id}">
			<jsp:invoke fragment="body" />
		</select>
	</div>
</div>