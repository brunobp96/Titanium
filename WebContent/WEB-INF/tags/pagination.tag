<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="previous" fragment="true" required="true"%>
<%@ attribute name="next" fragment="true" required="true"%>
<%@ attribute name="pages" fragment="true" required="true"%>

<nav aria-label="Pagination" class="text-center">
	<ul class="pagination">

		<jsp:invoke fragment="previous" />

		<jsp:invoke fragment="pages" />

		<jsp:invoke fragment="next" />

	</ul>
</nav>