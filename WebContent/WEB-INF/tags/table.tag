<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="body" fragment="true" required="true"%>
<%@ attribute name="id" required="false"%>

<div id="${id}" class="os-panel">
	<div class="os-title">
		<h2>
			<i class="fa fa-list"></i> Lista
		</h2>

		<ul class="nav navbar-right panel-toolbox">
			<li>
				<a data-toggle="collapse" href="#list-form" aria-expanded="true" class="collapse-link">
					<i class="fa fa-chevron-up os-toggle"></i>
				</a>
			</li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div id="list-form" class="os-content collapse in">

		<table class="table table-striped table-responsive">
			<jsp:invoke fragment="body" />
		</table>
	</div>
</div>