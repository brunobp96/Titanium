<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="bcFirst" required="true"%>
<%@ attribute name="bcLast" required="false"%>
<%@ attribute name="bcIcon" required="false"%>

<h2>
	<i class="fa ${bcIcon}"></i> ${bcFirst}
</h2>

<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a id="bcIndex" href="#">Main Page</a>
	</li>
	<c:choose>
		<c:when test="${empty bcLast}">
			<li class="breadcrumb-item active">${bcFirst}</li>
		</c:when>
		<c:otherwise>
			<li class="breadcrumb-item">
				<a id="bc${bcFirst}" href="#">${bcFirst}</a>
			</li>
			<li class="breadcrumb-item active">${bcLast}</li>
		</c:otherwise>
	</c:choose>
</ol>