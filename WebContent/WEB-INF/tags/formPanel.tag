<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="formIcon" required="true"%>
<%@ attribute name="formName" required="true"%>
<%@ attribute name="formDisplayName" required="true"%>
<%@ attribute name="formMethod" required="false"%>
<%@ attribute name="formBody" fragment="true" required="true"%>
<%@ attribute name="formId" required="false"%>
<%@ attribute name="formAction" required="true"%>
<%@ attribute name="formCssClasses" required="false"%>

<%@ attribute name="additionalFormAction" required="false"%>
<%@ attribute name="additionalFormName" required="false"%>
<%@ attribute name="additionalFormId" required="false"%>
<%@ attribute name="additionalFormBody" fragment="true" required="false"%>
<%@ attribute name="additionalFormBtnTarget" required="false"%>
<%@ attribute name="additionalFormBtnText" required="false"%>
<%@ attribute name="additionalFormBtnIcon" required="false"%>
<%@ attribute name="additionalFormBtnColor" required="false"%>

<div class="os-panel">
	<div class="os-title">
		<h2>
			<i class="fa ${formIcon}"></i> ${formDisplayName}
		</h2>
		<ul class="nav navbar-right panel-toolbox">
			<li>
				<a data-toggle="collapse" href="#div-${formId}" aria-expanded="true" class="collapse-link">
					<i class="fa fa-chevron-up os-toggle"></i>
				</a>
			</li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div id="div-${formId}" class="os-content collapse in">
		<form id="${formId}" action="${formAction}" class="${formCssClasses}" name="${formName}"
			method='<c:out escapeXml="true" default="post" value="${empty formMethod ? 'post' : formMethod}" />'>

			<jsp:invoke fragment="formBody" />

			<div class="row">
				<div class="clearfix"></div>
				<div class="hr-solid"></div>
			</div>

			<div style="display:none;" class="form-group pull-left os-margin-top-5 ">
				<button type="submit" class="btn btn-primary" value="Save">
					<i class="fa fa-floppy-o"></i> Salvar
				</button>
			</div>
		</form>

		<c:if test="${not empty additionalFormAction}">
			<form id="${additionalFormId}" class="pull-left" action="${additionalFormAction}" name="${additionalFormName}" method="post">
				<jsp:invoke fragment="additionalFormBody" />
				<a data-toggle="modal" data-target="#${additionalFormBtnTarget}"
					class="btn <c:out value="${empty additionalFormBtnIcon ? 'btn-danger' : additionalFormBtnIcon}" /> os-margin-5">
					<i class="fa <c:out value="${empty additionalFormBtnIcon ? 'fa-trash-o' : additionalFormBtnIcon}" />"></i>
					<c:out value="${empty additionalFormBtnText ? 'Remover' : additionalFormBtnText}" />
				</a>
			</form>
		</c:if>
	</div>
</div>