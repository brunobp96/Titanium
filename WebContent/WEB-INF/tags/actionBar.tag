<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="actionName" required="false"%>
<%@ attribute name="actionLink" required="false"%>
<%@ attribute name="actionReturn" required="false"%>

<c:choose>
	<c:when test="${not empty actionName && not empty actionLink}">
		<div class="row col-sm-12">
			<form action="${actionLink}" method="POST" class="pull-right os-margin-5">
				<button type="submit" value="${actionName}" class="btn btn-primary form-control">
					<i class="fa fa-plus"></i> ${actionName}
				</button>
				<input type="hidden" name="servletAction" value="prepareToCreate" />
			</form>
			<c:choose>
				<c:when test="${not empty actionReturn}">
					<form action="${actionReturn}" class="pull-right os-margin-5">
						<button type="submit" value="Voltar" class="btn btn-default form-control">
							<i class="fa fa-reply"></i> Voltar
						</button>
					</form>
				</c:when>
				<c:otherwise>
					<form action="employeeInterface.jsp" class="pull-right os-margin-5">
						<button type="submit" value="Voltar" class="btn btn-default form-control">
							<i class="fa fa-reply"></i> Voltar
						</button>
					</form>
				</c:otherwise>
			</c:choose>
		</div>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${empty actionReturn}">
				<form action="employeeInterface.jsp" class="pull-right os-margin-5">
					<button type="submit" value="Voltar" class="btn btn-default form-control">
						<i class="fa fa-reply"></i> Voltar
					</button>
				</form>
			</c:when>
			<c:otherwise>
				<form action="${actionReturn}" class="pull-right os-margin-5">
					<button type="submit" value="Voltar" class="btn btn-default form-control">
						<i class="fa fa-reply"></i> Voltar
					</button>
				</form>
			</c:otherwise>
		</c:choose>
	</c:otherwise>

</c:choose>