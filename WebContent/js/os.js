$("#card-alert .close").click(function() {
	$(this).closest('#card-alert').fadeOut('slow');
});

$(document).ready(
		function() {
			$('.os-content').on(
					'shown.bs.collapse',
					function() {
						$(this).parent().find(".fa-chevron-down").removeClass(
								"fa-chevron-down").addClass("fa-chevron-up");
					}).on(
					'hidden.bs.collapse',
					function() {
						$(this).parent().find(".fa-chevron-up").removeClass(
								"fa-chevron-up").addClass("fa-chevron-down");
					});
			
			$("#bcIndex").on("click", function() {
				backToIndex();
			});
			
			$('a').on("click", function(){
				//alert($(this).attr('id').replace('bc',''));
				// TODO: Submit respective form 
			});
			
			if(localStorage.getItem('continueTour') == "true"){
				helpAssistant();
			};
		});

function backToIndex() {
	// Based on Form Sidebar. If Sidebar is not loaded at page, the function
	// will not work.
	var indexForm = $("#employeeInterfaceForm");
	if (indexForm.length)
		indexForm.submit();
}

function backOnePage() {
	window.history.back();
}

function getCurrentMonth(literal) {
	if(!literal)
		return new Date().getMonth();
	
    var month = new Array();
    month[0] = "Janeiro";
    month[1] = "Fevereiro";
    month[2] = "Março";
    month[3] = "Abril";
    month[4] = "Maio";
    month[5] = "Junho";
    month[6] = "Julho";
    month[7] = "Agosto";
    month[8] = "Setembro";
    month[9] = "Outubro";
    month[10] = "Novembro";
    month[11] = "Dezembro";

    var d = new Date();
    var n = month[d.getMonth()];
    return n;
}

function getMonth(monthNumber){
	var month = new Array();
	month[0] = "Janeiro";
	month[1] = "Fevereiro";
	month[2] = "Março";
	month[3] = "Abril";
	month[4] = "Maio";
	month[5] = "Junho";
	month[6] = "Julho";
	month[7] = "Agosto";
	month[8] = "Setembro";
	month[9] = "Outubro";
	month[10] = "Novembro";
	month[11] = "Dezembro";
	
	return month[monthNumber];
}

/*
 * Initialize and executes the Tour Assistant on the page.
 * If the DOM element can't be found, the step into the array
 * will be ignored, and proceed to the next available DOM element.
 */
function helpAssistant()
{
	if(localStorage.getItem('tour_end') == "yes")
	{
		localStorage.removeItem('tour_current_step');
		localStorage.removeItem('tour_end');
		localStorage.removeItem('continueTour');
	}
	
	// Instance the tour
	var tour = new Tour(
	{
	  steps: [
      {
	    element: "#employee-level-title",
	    title: "Nível do funcionário",
	    content: "Esse é seu nível de acesso de usuário. Seu nível determina o acesso às funcionalidades."
      },	  
	  {
	    element: "#no-department-ticket-info",
	    title: "Tickets sem departamento",
	    content: "Você pode visualizar e navegar para tickets que ainda <u>não</u> estão atribuídos à um departamento."
	  },
	  {
	    element: "#my-department-ticket-info",
	    title: "Tickets de meu departamento",
	    content: "Você pode visualizar e navegar para tickets que estão atribuídos ao <u>seu departamento.</u>"
	  },
	  {
	    element: "#my-ticket-info",
	    title: "Meus Tickets atribuídos",
	    content: "Você pode visualizar e navegar para os tickets que estão atribuídos ao <u>seu usuário.</u>"
	  },
	  {
	    element: ".nav.navbar-nav",
	    title: "Menu Lateral",
	    content: "Você pode visualizar e navegar para as principais funcionalidade do sistema. A seguir iremos explicar as principais funcionalidades."
	  },
	  {
	    element: "#account-dropdown-nav",
	    title: "Sair do sistema",
	    content: "Caso seja preciso sair do sistema ou trocar de usuário, aqui você pode encerrar a sessão.",
	    onShow: function (tour) {
	    	$(function () {
    		  $('.dropdown-menu').dropdown('toggle');
    		});
	    },
	    onHidden: function (tour) {
	    	$(function () {
    		  $('.dropdown-menu').dropdown('toggle');
    		});
	    }
	  },
	  {
	    orphan: true,
	    title: "Obsidian Sphere - Ajuda",
	    content: "Essas são as orientações básicas do sistema. Prossiga para ver as funcionalidades específicas disponíveis.",
	    onNext: function(){
	    	localStorage.setItem('continueTour', true);
	        document.location.href = '/ObsidianSphere/employeeTickets.jsp';
	    }
	  },
	  {
		element: "#filter-panel",
	    title: "Painel de Filtros",
	    content: "Você pode utilizar os filtros para realizar uma busca",
	    backdropContainer: 'body',
	    placement: 'auto'
	  },
	  {
		element: "#content-panel",
	    title: "Painel de Tickets",
	    content: "Essa é a lista com seus tickets e você pode realizar algumas ações.",
	    backdropContainer: 'body',
	    placement: 'auto'
	  }
	],
	//storage: false, // Disable so it can execute every time user clicks on "Ajuda" on leftSidebar
	smartPlacement: true,
	backdrop: true, // Better highlight on Obsidian Sphere sidebar
	container: "body",
	backdropContainer: 'body>nav', 
	basePath: "",
	template: "<div class='popover tour'>\
	    <div class='arrow'></div>\
	    <h3 class='popover-title'></h3>\
	    <div class='popover-content'></div>\
		    <div class='popover-navigation'>\
		        <div class='btn-group'>\
		        	<button class='btn btn-default' data-role='prev'>«</button>\
		        	<button class='btn btn-default' data-role='next'>»</button>\
	    		</div>\
	    		<button class='btn btn-default' data-role='end'>X</button>\
			</div>\
	  </div>",
	  onEnd: function(tour){
		  localStorage.setItem('continueTour', false);
	  }
	});

	// Initialize the tour
	tour.init();

	tour.start();
}


