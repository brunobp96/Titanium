<!-- Meta Configurations -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<!--  Page Configurations  -->
<title>Titanium</title>

<link rel="shortcut icon" href="images/logo.png">

<!-- Cascade Style Sheets -->
<link rel="stylesheet" href="css/jquery-ui/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css" />
<link rel="stylesheet" href="css/font-awesome.min.css" />
<link rel="stylesheet" href="css/timepickerTable.css" />
<link rel="stylesheet" href="css/animate.css" />
<link rel="stylesheet" href="css/os.css" />
<link rel="stylesheet" type="text/css" href="css/sidebar.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap-multiselect.css" />
<link rel="stylesheet" href="css/bootstrap/bootstrap-tour.min.css" >

<!-- Java Scripts -->
<script type="text/javascript" src="js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="js/bootstrap/bootstrap-multiselect.js"></script>
<script src="js/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript" src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/sidebar.js"></script>
<script src="js/jquery/jquery.mask.min.js"></script>
<script src="js/bootstrap/bootstrap-tour.min.js"></script>
<script src="js/os.js"></script>

<script type="text/javascript" src="js/moment.min.js"></script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>