<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="css/login.css" />
		<jsp:include page="header.jsp"></jsp:include>
	</head>
<body>
<div class="container">
	<div class="card card-container text-center">
		<img class="profile-img-card image-center spin" src="images/logo.png" />
	    <form class="form-signin" action="LoginService" method="post" >
	    	<h2>Titanium</h2>
	    	<div class="form-group has-feedback">
	   	 		<input type="text" class="form-control" value="${email}" placeholder="E-mail" name="email" id="email" required autofocus/>
	   	 	</div>
	   	 	<div class="form-group has-feedback">
	    		<input type="password" class="form-control" value="" placeholder="Password" name="password" id="password" required/>
	   		</div>
	    	<button class="btn btn-lg btn-obsidian btn-block btn-signin" type="submit">Login</button>
	    	<a href="register.jsp" style="height:30px;" type="submit">Sign up</a>
	    </form>
	    ${error}
	</div>
	<div class="text-center white">
		<jsp:useBean id="date" class="java.util.Date" />
		<fmt:formatDate value="${date}" pattern="yyyy" /> - Titanium
	</div>
</div>

</body>

</html>