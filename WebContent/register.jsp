<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="h"%>
<jsp:include page="header.jsp"></jsp:include>

</head>
<body>
<div class="container">
	<div class="col-lg-12 col-md-12 well">
		<div class="row">
			<h2>
				<i class="fa fa-users"></i> New User
			</h2>
			<div class="clearfix"></div>
			<div class="hr-solid"></div>
			<div class="padding-right:10px;padding-left:10px;" class="row">
				<h:input icon="fa-envelope" type="text" id="email" fieldName="E-mail" displayName="E-mail" htmlAttributes="required" toolTip="Your e-mail" value="${email}" width="col-sm-12 col-md-12"></h:input>
				<h:input icon="fa-lock" type="password" id="password" fieldName="password" displayName="Password" htmlAttributes="required maxlength='55'" toolTip="Password" width="col-sm-12 col-md-12"></h:input>
				<h:input icon="fa-lock" type="password" id="passwordTest" fieldName="passwordTest" displayName="Confirm" htmlAttributes="required maxlength='55'" toolTip="Confirm" width="col-sm-12 col-md-12"></h:input>
			</div>
		</div>
		<div id="displayError" style="display:none;" class="col-lg-12 col-md-12 alert alert-danger" role="alert">
			<i class="fa fa-exclamation"></i>
			<label id="error"></label>
		</div>
		<div id="displaySuccess" style="display:none;" class="col-lg-12 col-md-12 alert alert-success" role="alert">
			<i class="fa fa-check-circle-o"></i> The user was successfully created.
		</div>
		
	
		<div class="row" class="padding-right:10px;padding-left:10px;">
			<div class="form-group pull-left">
				<a href="login.jsp" class="btn btn-primary" >
					<i class="fa fa-arrow-circle-o-left"></i> Return
				</a>
			</div>
			
			<div class="form-group pull-right">
				<a id="save" class="btn btn-primary" >
					<i class="fa fa-floppy-o"></i> Save
				</a>
			</div>
		</div>
		
	</div>
</div>
	<script>
	
		$(function() {
			$("#save").click(function() {
				
				var email = $("#email").val().trim();
				var password = $("#password").val().trim();
				var passwordTest = $("#passwordTest").val().trim();
				
				var errorMessage = "Please, fill the following fields: ";
				var showError = false;
				
				if(email == ""){
					$("#displayError").show();
					errorMessage += "- E-mail "
					var showError = true;
				}else if(!showError){
					$("#displayError").hide();
				}
				
				if(password == ""){
					$("#displayError").show();
					errorMessage += "- Password"
					var showError = true;
				}else if(!showError){
					$("#displayError").hide();
				}
				
				if(passwordTest == ""){
					$("#displayError").show();
					errorMessage += "- Verification Password"
					var showError = true;
				}else if(!showError){
					$("#displayError").hide();
				}
				
				$("#error").text(errorMessage);
				
				if(password != passwordTest){
					$("#displayError").show();
					$("#error").text("The password must match.");
					var showError = true;
				}else if(!showError){
					$("#displayError").hide();
				}
				
				if(!showError){
					$.ajax({
						url : "UserService",
						type : "POST",
						data : {
							action : "Register",
							email : email,
							password : password
						},
						dataType : "json",
						success : function(data) {
							if(data == true){
								$("#displaySuccess").show();
								$("#displayError").hide();
							}else{
								$("#displayError").show();
								$("#error").text("This e-mail is already in use, please try another.");
								
								$("#displaySuccess").hide();
							}
							
						}
					});
				}
				
			});
		});

	</script>
</body>
</html>