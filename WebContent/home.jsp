<%@page language="java" contentType="text/html; charset=ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib tagdir="/WEB-INF/tags" prefix="h"%>
<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="/UserTasks" />

<style type="text/css">
 {
    box-sizing: border-box;
}

ul li {
    cursor: pointer;
    position: relative;
    padding: 12px 8px 12px 40px;
    background: #eee;
    font-size: 18px;
    transition: 0.2s;

    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

ul li:nth-child(odd) {
    background: #f9f9f9;
}

ul li:hover {
    background: #ddd;
}

ul li.checked {
    background: #888;
    color: #fff;
    text-decoration: line-through;
}

ul li.checked::before {
    content: '';
    position: absolute;
    border-color: #fff;
    border-style: solid;
    border-width: 0 2px 2px 0;
    top: 10px;
    left: 16px;
    transform: rotate(45deg);
    height: 15px;
    width: 7px;
}

.close {
    position: absolute;
    right: 0;
    top: 0;
    padding: 12px 16px 12px 16px;
}

.close:hover {
    background-color: #f44336;
    color: white;
}

.addBtn {
    padding: 10px;
    width: 25%;
    background: #d9d9d9;
    color: #555;
    float: left;
    text-align: center;
    font-size: 16px;
    cursor: pointer;
    transition: 0.3s;
}

.addBtn:hover {
    background-color: #bbb;
}
</style>
</head>
<body>

<nav class="navbar navbar-inverse " role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<span class="navbar-brand">Welcome user: ${sessionScope.employeeSession.email}</span>
		</div>
		<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
			<div class="col-lg-1 pull-right" style="padding-top: 7px;">
				<form action="LogoutService" method="get">
					<button type="submit" class="btn btn-info"><i class="fa fa-sign-out" style="color: white;" aria-hidden="true"></i>Logout</button>
				</form>
			</div>
		</div>
		
	</div>
</nav>


<div class="container">
	<div class="col-lg-12 col-md-12 well">
		<div id="mainDiv" class="header col-lg-12 col-md-12" >
	 		<h2>User's ${sessionScope.employeeSession.email} To Do List</h2>
			<input type="text" id="newTast" placeholder="New Task" style="border: none; width: 75%; padding: 10px; float: left; font-size: 16px;">
			<span onclick="newElement()" class="addBtn">Add</span>
		</div>
		
		<br>
		
		<div id="error" style="display:none; text-align: center; padding: 10px" class="col-lg-12 col-md-12 alert alert-danger" role="alert">
			<i class="fa fa-exclamation"></i>
			<label >Please fill the task field above.</label>
		</div>
		
		<br>
		
		
		<ul class="col-lg-12 col-md-12" style="padding:10px; margin: 0; padding: 0;" id="listing">
		
			<c:forEach var="taskList" items="${taskList}">
				<c:choose>
					<c:when test="${taskList.status == 1}">
						<li id="${taskList.id}" class="task" >${taskList.description}</li>
					</c:when>
					<c:otherwise>
						<li id="${taskList.id}" class="checked task">${taskList.description}</li>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</ul> 
		
	</div>
</div>
	<script>

		var myNodelist = document.getElementsByTagName("LI");
		var i;
		for (i = 0; i < myNodelist.length; i++) {
			var span = document.createElement("SPAN");
			var txt = document.createTextNode("\u00D7");
			span.className = "close";
			span.appendChild(txt);
			myNodelist[i].appendChild(span);
		}

		var close = document.getElementsByClassName("close");
		var i;
		for (i = 0; i < close.length; i++) {
			close[i].onclick = function() {
				var div = this.parentElement;
				div.style.display = "none";
			}
		}

		var list = document.querySelector('ul');
		list.addEventListener('click', function(ev) {
			if (ev.target.tagName === 'LI') {
				ev.target.classList.toggle('checked');
			}
		}, false);
		
		$(".task").click(function() {
			
			var id = this.id;
			var status = 0;
			
		    if($(this).hasClass('checked')){
		    	status = 1;
		    }else{
		    	status = 0;
		    }
		    
			$.ajax({
				url : "TaskService",
				type : "POST",
				data : {
					action : "Update",
					task : '',
					id: id,
					status: status,
				},
				dataType : "json",
			});
		    
		});
		
		$(".close").click(function() {
			
			var id = $(this).closest("li").attr('id');
			
			$.ajax({
				url : "TaskService",
				type : "POST",
				data : {
					action : "Delete",
					task : '',
					id: id,
				},
				dataType : "json",
			});
		    
		});

		function newElement() {
			var li = document.createElement("li");
			var inputValue = document.getElementById("newTast").value;
			var t = document.createTextNode(inputValue);
			li.appendChild(t);
			
			if (inputValue === '') {
				$("#error").show();
			} else {
				$("#error").hide();
				
				$.ajax({
					url : "TaskService",
					type : "POST",
					data : {
						action : "Register",
						task : inputValue
					},
					dataType : "json",
					success : function(data) {
						
						li.setAttribute("id", data);
						li.setAttribute("class", "task");
						
						document.getElementById("listing").appendChild(li);
						
					}
				});
			}
			document.getElementById("newTast").value = "";

			var span = document.createElement("SPAN");
			var txt = document.createTextNode("\u00D7");
			span.className = "close";
			span.appendChild(txt);
			li.appendChild(span);

			for (i = 0; i < close.length; i++) {
				close[i].onclick = function() {
					var div = this.parentElement;
					div.style.display = "none";
				}
			}
		}
	</script>
</body>
</html>